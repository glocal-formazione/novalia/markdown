# Markdown
I think I'll use it to format all of my documents from now on.	

## I Titoli

### Titolo 3

### Titolo 4

##### Titolo 5

---

## Paragrafi

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent malesuada vulputate lectus, quis facilisis arcu laoreet ac.

Praesent at orci pellentesque, consectetur metus vel, placerat arcu.

Donec laoreet, lacus sit amet gravida aliquam, nulla nisl finibus mi, a commodo lectus turpis finibus tellus. Nulla placerat tempus vestibulum. In hac habitasse platea dictumst.

Fusce finibus metus ut accumsan dictum. Integer eget metus et lectus viverra varius. Etiam nec pulvinar justo, at dapibus magna. Etiam augue mi, cursus et felis ac, venenatis maximus risus.

----

## Enfasi

### Grassetti
Sed a elementum nunc, ut **porttitor elit**. Sed dui dolor, sollicitudin et felis et, condimentum varius velit. Curabitur posuere sodales accumsan. Sed ultricies metus quis elit rutrum, pretium feugiat nulla faucibus.

### Corsivo

Mauris semper, *étortor id malesuada* luctus, nunc lectus posuere orci, id faucibus purus risus quis odio. Nunc at ornare nibh, eget blandit sem. Donec sit amet lectus a massa elementum mattis in eget nulla. Nulla facilisi.

### Corsivo & Grassetto

Mauris semper, ***étortor id malesuada*** luctus, nunc lectus posuere orci, id faucibus purus risus quis odio. Nunc at ornare nibh, eget blandit sem. Donec sit amet lectus a massa elementum mattis in eget nulla. Nulla facilisi.

Mauris semper, ~~étortor id malesuada~~ luctus, nunc lectus posuere orci, id faucibus purus risus quis odio. Nunc at ornare nibh, eget blandit sem. Donec sit amet lectus a massa elementum mattis in eget nulla. Nulla facilisi.

----

## Blockquote

> Mauris semper, tortor id malesuada luctus, nunc lectus posuere orci, id faucibus purus risus quis odio. Nunc at ornare nibh, eget blandit sem. Donec sit amet lectus a massa elementum mattis in eget nulla. Nulla facilisi.

### Paragrafi multipli

> Mauris semper, tortor id malesuada luctus, nunc lectus posuere orci, id faucibus purus risus quis odio.
>
> Nunc at ornare nibh, eget blandit sem. Donec sit amet lectus a massa elementum mattis in eget nulla. Nulla facilisi.

### Nested

> 
> Mauris semper, tortor id malesuada luctus, nunc lectus posuere orci, id faucibus purus risus quis odio.
>
> > Nunc at ornare nibh, eget blandit sem. Donec sit amet lectus a massa elementum mattis in eget nulla. Nulla facilisi.

----

## Liste

### Ordinate

1. list
2. list
3. list


### Non ordinate

- list
- list
  - list
  - list
- list

---

## Immagini

![Tux, the Linux mascot](/assets/tux.png)

---

## Link
This is the [Markdown Guide](https://www.markdownguide.org).

| Tabella | Tabllea |
|---------|------|
| Test | Test |
| Test | Test |